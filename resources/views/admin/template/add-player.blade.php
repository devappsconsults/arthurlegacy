@extends('admin/layout/main')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Player</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Player</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Player Details
              </h3>
              @if(isset($player))
                <div class="alert alert-success">{{ $player->name }} has been saved</div>
              @endif
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
            <form class="form" id="form"  method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputEmail1">Player Name</label>
                    <input type="text" class="form-control"  name="name" id="exampleInputEmail1" placeholder="Player Name" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Player Team</label>
                    <input type="text" class="form-control" name="team" id="exampleInputEmail1" placeholder="Player Team" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Player Position</label>
                    <input type="text" class="form-control" name="position" id="exampleInputEmail2" placeholder="Player Position" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Player Type</label>
                        <select name="type" class="form-control">
                          <option value="current">Current Player</option>
                          <option value="historical">Historical Player</option>
                        </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Player Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file" id="exampleInputFile" required>
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Add Player</button>
            </form>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
Use Form data

$("#form").onclick(
    function (event) {
        event.preventDefault();
        var formData = new FormData('form');
        $.ajax({
            type: 'post',
            async: false,
            cache: false,
            url: '',
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            success: function (response) {

            }
        });
    }
);

</script>
@endsection
