@extends('admin/layout/main')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add News</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add News</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
              News Details
              </h3>
              @if(isset($news))
                <div class="alert alert-success">{{ $news->title }} has been saved</div>
              @endif
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
            <form class="form" id="form"  method="POST" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="exampleInputEmail1">News Title</label>
                    <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="News title" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile">News Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file" id="exampleInputFile" required>
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                </div>
                <div class="mb-3">
                    <textarea class="textarea" name="content" placeholder="Place some text here"
                          style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required></textarea>
                </div>

                <button type="submit" class="btn btn-primary">Add News</button>
                </form>

            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<!-- Summernote -->
<script src="{{ env('APP_URL') }}plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
