@include('admin/layout/header')
@include('admin/layout/footer')
@include('admin/layout/sidenav')

<!DOCTYPE html>
<html>
    @yield('header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  @yield('mainnav')
  @yield('sidenav')
  @yield('content')
  @yield('footer')
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ env('APP_URL') }}plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ env('APP_URL') }}plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="{{ env('APP_URL') }}plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ env('APP_URL') }}plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- AdminLTE App -->
<script src="{{ env('APP_URL') }}dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ env('APP_URL') }}dist/js/demo.js"></script>
<!-- page script -->

</body>
@yield('script')

</html>
