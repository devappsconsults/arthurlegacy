<!DOCTYPE html>
<html lang="en-gb">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ArthurLegacy : Agency</title>
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="{{ env('APP_URL') }}images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="{{ env('APP_URL') }}css/akslider.css" rel="stylesheet" type="text/css" />
    <link href="{{ env('APP_URL') }}css/donate.css" rel="stylesheet" type="text/css" />
    <link href="{{ env('APP_URL') }}css/theme.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/mootools/1.3.1/mootools-yui-compressed.js'></script>

    <script src="{{ env('APP_URL') }}https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
</head>

<body class="tm-isblog">

    <div class="preloader">
        <div class="loader"></div>
    </div>


    <div class="over-wrap">
        <div class="toolbar-wrap">
            <div class="uk-container uk-container-center">
                <div class="tm-toolbar uk-clearfix uk-hidden-small">


                    <div class="uk-float-right">
                        <div class="uk-panel">
                            <div class="social-top">
                                <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-facebook"></span></a>
                                <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-twitter"></span></a>
                                <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-google"></span></a>
                                <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-pinterest"></span></a>
                                <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-youtube"></span></a>
                                <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-instagram"></span></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="tm-menu-box">

            <div style="height: 70px;" class="uk-sticky-placeholder">
                <nav style="margin: 0px;" class="tm-navbar uk-navbar" data-uk-sticky="">
                    <div class="uk-container uk-container-center">
                        <a class="tm-logo uk-float-left" href="{{ env('APP_URL') }}#home">
                            <img src="{{ env('APP_URL') }}images/logo.png" style="max-height:60px" alt="logo" title="logo">
                        </a>

                        <ul class="uk-navbar-nav uk-hidden-small">
                            <li ><a href="{{ env('APP_URL') }}#home">Home</a></li>
                            <li ><a href="{{ env('APP_URL') }}#about">About</a></li>
                            <li ><a href="{{ env('APP_URL') }}#team">Team</a></li>
                            <li><a href="{{ env('APP_URL') }}#players">Players</a></li>
                            <li><a href="{{ env('APP_URL') }}#services">Services</a></li>
                            <li><a href="{{ env('APP_URL') }}#news">News</a></li>
                            <li><a href="{{ env('APP_URL') }}#contact">Contact</a></li>
                        </ul>
                        <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas=""></a>
                    </div>
                </nav>
            </div>

        </div>

        <div class="tm-top-a-box tm-full-width tm-box-bg-1 ">
            <div class="uk-container uk-container-center">
                <section id="tm-top-a" class="tm-top-a uk-grid uk-grid-collapse" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                    <div class="uk-width-1-1 uk-row-first">
                        <div class="uk-panel">
                            <div class="uk-cover-background uk-position-relative head-wrap" style="height: 290px; background-image: url('images/head-bg.jpg');">
                                <img class="uk-invisible" src="{{ env('APP_URL') }}images/head-bg.jpg" alt="" height="290" width="1920">
                                <div class="uk-position-cover uk-flex uk-flex-center head-title">
                                    <h1>
                                        News
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>


        <div class="uk-container uk-container-center alt">
            <ul class="uk-breadcrumb">
                <li><a href="{{ env('APP_URL') }}">Home</a></li>
                <li class="uk-active"><span>All News</span></li>
            </ul>
        </div>

        <div class="uk-container uk-container-center">
            <div id="tm-middle" class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
                <div class="tm-main uk-width-medium-3-4 uk-push-1-4">

                    <div class="contentpaneopen">
                       <main id="tm-content" class="tm-content">
                            <div class="uk-grid" data-uk-grid-match="">
                                @foreach($news as $_news)
                                    <div class="uk-width-large-1-3 uk-width-medium-2-4 uk-width-small-2-4 list-article uk-flex uk-flex-column">
                                        <div class="wrapper">
                                            <div class="img-wrap uk-flex-wrap-top">
                                                <a href="{{ env('APP_URL') }}single-news?my-news-id={{ $_news->id }}">
                                                <img src="data:{{ $_news->image_type }};base64, {{$_news->image}}" class="img-polaroid" alt="">
                                                </a>
                                            </div>
                                            <div class="info uk-flex-wrap-middle">
                                                <div class="date">
                                                    {{ date('M d, Y', strtotime($_news->created_at)) }}
                                                </div>
                                                <div class="name">
                                                    <h4>
                                                    <a href="{{ env('APP_URL') }}single-news?my-news-id={{ $_news->id }}">
                                                        {{ $_news->short_title }}
                                                    </a>
                                                    </h4>
                                                </div>
                                                <div class="text">
                                                    <p>
                                                    {{ $_news->short_content }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="article-actions uk-flex-wrap-bottom">
                                            <div class="count"><i class="uk-icon"></i><span></span></div>
                                            <div class="read-more"><a href="{{ env('APP_NAME') }}single-news">Read More</a></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!--
                            <form method="post">
                                <div class="pagination">
                                    <ul class="pagination-list">
                                        <li class="pagination-start"><span class="pagenav">Start</span></li>
                                        <li class="pagination-prev"><span class="pagenav">Prev</span></li>
                                        <li><span class="pagenav">1</span></li>
                                        <li><a href="#" class="pagenav">2</a></li>
                                        <li class="pagination-next"><a data-original-title="Next" title="" href="#" class="hasTooltip pagenav">Next</a></li>
                                        <li class="pagination-end"><a data-original-title="End" title="" href="#" class="hasTooltip pagenav">End</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                            -->
                        </main>
                    </div>


                </div>
                <aside class="tm-sidebar-a uk-width-medium-1-4 uk-pull-3-4 uk-row-first">
                    <div class="uk-panel news-sidebar">
                        <h3 class="uk-panel-title">Latest News</h3>
                        @foreach($latestNews as $latest)
                            <article class="has-context ">
                                <div class="latest-news-wrap">
                                    <div class="img-wrap">
                                        <a href="{{ env('APP_URL') }}single-news?my-news-id={{ $latest->id }}">
                                        <img src="data:{{ $latest->image_type }};base64, {{$latest->image}}" class="img-polaroid" alt="">
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="date">
                                        {{ date('M d, Y', strtotime($latest->created_at)) }}
                                        </div>
                                        <div class="name">
                                            <h4>
                                                <a href="{{ env('APP_URL') }}single-news?my-news-id={{ $latest->id }}">
                                                {{ $latest->short_title }}
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </article>
                       @endforeach
                    </div>
                </aside>
            </div>
        </div>


        <div class="bottom-wrapper">
            <img draggable="false" class="uk-responsive-height" src="{{ env('APP_URL') }}images/partners.jpg" alt="" style="padding-bottom:40px">
            <div class="tm-bottom-f-box  ">
                <div class="uk-container uk-container-center">
                    <section id="tm-bottom-f" class="tm-bottom-f uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">

                        <div class="uk-width-1-1">
                            <div class="uk-panel">
                                <div class="footer-logo">
                                    <a href="../index.html"><img src="{{ env('APP_URL') }}images/logo.png" style="max-height:50px" alt=""></a>
                                </div>
                                <div class="footer-socials">
                                    <div class="social-top">
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-facebook"></span></a>
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-twitter"></span></a>
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-google"></span></a>
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-pinterest"></span></a>
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-youtube"></span></a>
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-instagram"></span></a>
                                        <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-flickr"></span></a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <footer id="tm-footer" class="tm-footer">


                <div class="uk-panel">
                    <div class="uk-container uk-container-center">
                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <div class="footer-wrap">
                                    <div class="foot-menu-wrap">
                                        <ul class="nav menu">
                                            <li class="item-165"><a href="{{ env('APP_URL') }}#about">About</a>
                                            </li>
                                            <li class="item-166"><a href="{{ env('APP_URL') }}#players">Players</a>
                                            </li>
                                            <li class="item-167"><a href="{{ env('APP_URL') }}#team">Team</a>
                                            </li>
                                            <li class="item-168"><a href="{{ env('APP_URL') }}#services">Services</a>
                                            </li>
                                            <li class="item-169"><a href="{{ env('APP_URL') }}#news">News</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="copyrights">Copyright ©  <a href="{{ env('APP_URL') }}#home">ArthurLegacy</a>.</div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>

        <div id="offcanvas" class="uk-offcanvas">
            <div class="uk-offcanvas-bar">
                <ul class="uk-nav uk-nav-offcanvas">
                <li ><a href="{{ env('APP_URL') }}#home">Home</a></li>
                            <li ><a href="#about">About</a></li>
                            <li ><a href="#team">Team</a></li>
                            <li><a href="#players">Players</a></li>
                            <li><a href="#services">Services</a></li>
                            <li><a href="#news">News</a></li>
                            <li><a href="#contact">Contact</a></li>

                </ul>
            </div>
        </div>
    </div>



<script type="text/javascript" src="{{ env('APP_URL') }}js/jquery.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/uikit.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/SimpleCounter.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/grid.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/slider.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/slideshow.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/slideset.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/sticky.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/lightbox.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/components/accordion.js"></script>
<script type="text/javascript" src="{{ env('APP_URL') }}js/isotope.pkgd.min.js"></script>

<script type="text/javascript" src="{{ env('APP_URL') }}js/theme.js"></script>
<script type="text/javascript">
    new SimpleCounter("countdown4", 1447448400, {
      'continue': 0,
      format: '{D} {H} {M} {S}',
      lang: {
          d: {
              single: 'day',
              plural: 'days'
          }, //days
          h: {
              single: 'hr',
              plural: 'hrs'
          }, //hours
          m: {
              single: 'min',
              plural: 'min'
          }, //minutes
          s: {
              single: 'sec',
              plural: 'sec'
          } //seconds
      },
      formats: {
          full: "<span class='countdown_number' style='color:  '>{number} </span> <span class='countdown_word' style='color:  '>{word}</span> <span class='countdown_separator'>:</span>", //Format for full units representation
          shrt: "<span class='countdown_number' style='color:  '>{number} </span>" //Format for short unit representation
      }
  });
</script>

</body>

</html>
<!-- Localized -->
