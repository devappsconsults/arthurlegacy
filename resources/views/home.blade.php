<!DOCTYPE html>
<html lang="en-gb">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Arthur Legacy : Football Agency</title>
    <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="css/akslider.css" rel="stylesheet" type="text/css"/>
    <link href="css/donate.css" rel="stylesheet" type="text/css"/>
    <link href="css/theme.css" rel="stylesheet" type="text/css"/>
    <script type='text/javascript'
            src='http://ajax.googleapis.com/ajax/libs/mootools/1.3.1/mootools-yui-compressed.js'></script>

    <script
        src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyADqDmqDyciHVYYtls5_1l636zD_1kcQWo"></script>
</head>

<body class="tm-isblog">

<div class="preloader">
    <div class="loader"></div>
</div>


<div class="over-wrap">
    <div class="toolbar-wrap">
        <div class="uk-container uk-container-center">
            <div class="tm-toolbar uk-clearfix uk-hidden-small">


                <div class="uk-float-right">
                    <div class="uk-panel">
                        <div class="social-top">
                            <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-facebook"></span></a>
                            <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-twitter"></span></a>
                            <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-google"></span></a>
                            <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-pinterest"></span></a>
                            <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-youtube"></span></a>
                            <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-instagram"></span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="tm-menu-box">

        <div style="height: 70px;" class="uk-sticky-placeholder">
            <nav style="margin: 0px;" class="tm-navbar uk-navbar" data-uk-sticky="">
                <div class="uk-container uk-container-center">
                    <a class="tm-logo uk-float-left" href="#home">
                        <img src="images/logo.png" style="max-height:60px" alt="logo" title="logo">
                    </a>

                    <ul class="uk-navbar-nav uk-hidden-small">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#players">Players</a></li>
                        <li><a href="#services">Services</a></li>
                        <li><a href="#news">News</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                    <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas=""></a>
                </div>
            </nav>
        </div>

    </div>

    <div class="tm-top-a-box tm-full-width  " id="home">
        <div class="uk-container uk-container-center">
            <section id="tm-top-a" class="tm-top-a uk-grid uk-grid-collapse"
                     data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                <div class="uk-width-1-1">
                    <div class="uk-panel">
                        <div class="akslider-module ">
                            <div class="uk-slidenav-position"
                                 data-uk-slideshow="{height: 'auto', animation: 'swipe', duration: '500', autoplay: true, autoplayInterval: '5000', videoautoplay: true, videomute: true, kenburns: false}">
                                <ul class="uk-slideshow uk-overlay-active">
                                    <li aria-hidden="false" class="uk-height-viewport uk-active">
                                        <div style="background-image: url(images/main-slider-img.jpg);"
                                             class="uk-cover-background uk-position-cover"></div>
                                        <img style="width: 100%; height: auto; opacity: 0;" class="uk-invisible"
                                             src="images/main-slider-img.jpg" alt="">
                                    </li>
                                    <li aria-hidden="true" class="uk-height-viewport">
                                        <div style="background-image: url(images/main-slider-img1-.jpg);"
                                             class="uk-cover-background uk-position-cover"></div>
                                        <img style="width: 100%; height: auto; opacity: 0;" class="uk-invisible"
                                             src="images/main-slider-img1.jpg" alt="">

                                    </li>
                                </ul>
                                <a href="../index.html" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous"
                                   data-uk-slideshow-item="previous"></a>
                                <a href="../index.html" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next"
                                   data-uk-slideshow-item="next"></a>
                                <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-text-center">
                                    <li class="uk-active" data-uk-slideshow-item="0"><a href="../index.html">0</a>
                                    </li>
                                    <li data-uk-slideshow-item="1"><a href="../index.html">1</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="tm-top-d-box  " id="about">
        <div class="uk-container uk-container-center">
            <section id="tm-top-d" class="tm-top-d uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}"
                     data-uk-grid-margin="">

                <div class="uk-width-1-1">
                    <div class="uk-panel">
                        <div class="donate-wrap">
                            <div class="donate-inner">
                                <h3><span>About</span> Us</h3>
                                <div class="uk-grid">
                                    <div class="uk-width-8-10 uk-push-1-10 intro-text" style="text-align: left;">
                                        <p>
                                            Founded in 2014 by Oliver Arthur, one of Ghana’s leading football agents,
                                            ArthurLegacy Sports provides end to end training and management of young
                                            gifted African football talents.
                                        </p>
                                        <p>
                                            By organizing tournaments and staging friendly matches, The Agency scouts
                                            the best and brightest footballers from the continent and secures lucrative
                                            deals and transfers for them in world-class football clubs across Europe.
                                        </p>
                                        <p>
                                            Through these tournaments, the agency has successfully negotiated a number
                                            of transfer deals for notable players including Richmond Boakye-Yiadom
                                            (Genoa), Afriyie Acquah (Palermo), Godfred Donsah (Hellas Verona), Maxwell
                                            Aboagye Acosty (Fiorentina), Chibsah Rahman (Sassuolo) and Godfred Adofo
                                            (Parma).
                                        </p>
                                        <p>
                                            Outside of these tournament transfers, the agency has also secured direct
                                            player placements abroad. Outstanding players such as Eyram Leveh (Chievo
                                            Verona), John Kofi Essel (Cluj Napocea), Augustine Okra (BK Hacken), Bright
                                            Addae (Parma), Derrick Appiah (AC Monaco) and Patrick Asmah (Atalanta) are
                                            making great impact in the football clubs the belong to.
                                        </p>
                                        <p>
                                            As at July 2019, the agency has brokered over fifty (50) direct and indirect
                                            deals for African football players from diverse backgrounds, ethnicity and
                                            culture.
                                        </p>
                                        <p>
                                            Currently, ArthurLegacy Sports is managing over fifteen (15) young talents,
                                            three of whom are Nigerians: Orji Okwonkow (Striker, Bologna), Kingsley
                                            Michael Dogo (Midfielder, Bologna), and Sadiq Umar (A.S Roma). These players
                                            are highly regarded in their respective clubs.
                                        </p>
                                    </div>
                                </div>
                                <form class="teamdonate-form" method="post" target="paypal">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <div class="tm-top-e-box tm-full-width  va-main-next-match" id="team">
        <div class="uk-container uk-container-center">
            <section id="tm-top-e" class="tm-top-e uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}"
                     data-uk-grid-margin="">

                <div class="uk-width-1-1">
                    <div class="uk-panel">
                        <div class="our-team-main-wrap">
                            <div class="uk-container uk-container-center">
                                <div class="uk-grid" data-uk-grid-match="">
                                    <div class="uk-width-medium-8-10 uk-width-small-1-1 uk-push-1-10">
                                        <div class="our-team-wrap">
                                            <div class="our-team-title">
                                                <h3>Our <span style="color:white">Team</span></h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_2a195f12da9f3f36da06e6097be7e451">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <div class="img-wrap">
                                                    <a href="#">
                                                        <img src="images/oliver.jpg" class="img-polaroid"
                                                             alt="Steven Webb" title="Steven Webb">
                                                    </a>
                                                    <ul class="socials">
                                                        <li class="twitter">
                                                            <a href="http://twitter.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="facebook">
                                                            <a href="http://facebook.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="google-plus">
                                                            <a href="https://plus.google.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="pinterest">
                                                            <a href="https://www.pinterest.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="linkedin">
                                                            <a href="https://www.linkedin.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="info">
                                                    <div class="name">
                                                        <h3>
                                                            <a href="#">OLIVER ARTHUR </a>
                                                        </h3>
                                                    </div>
                                                    <div class="position">Official Intermediary</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_22c19cd174143e3b4c7ef40ae23c5d1a">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <div class="img-wrap">
                                                    <a href="#">
                                                        <img src="images/gideon.jpg" class="img-polaroid"
                                                             alt="John Montgomery" title="John Montgomery">
                                                    </a>
                                                    <ul class="socials">
                                                        <li class="twitter">
                                                            <a href="http://twitter.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="facebook">
                                                            <a href="http://facebook.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="google-plus">
                                                            <a href="https://plus.google.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="pinterest">
                                                            <a href="https://www.pinterest.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="linkedin">
                                                            <a href="https://www.linkedin.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="info">
                                                    <div class="name">
                                                        <h3>
                                                            <a href="#">GIDEON ATTOH </a>
                                                        </h3>
                                                    </div>
                                                    <div class="position">Chief Scout</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_81747b4426a9882884c1f83eda78844f">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <div class="img-wrap">
                                                    <a href="#">
                                                        <img src="images/kate.jpg" class="img-polaroid"
                                                             alt="Johnny Thompson" title="Johnny Thompson">
                                                    </a>
                                                    <ul class="socials">
                                                        <li class="twitter">
                                                            <a href="http://twitter.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="facebook">
                                                            <a href="http://facebook.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="google-plus">
                                                            <a href="https://plus.google.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="pinterest">
                                                            <a href="https://www.pinterest.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="linkedin">
                                                            <a href="https://www.linkedin.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="info">
                                                    <div class="name">
                                                        <h3><a href="#">KATHLEEN ARTHUR</a></h3>
                                                    </div>
                                                    <div class="position">Chief Executive Officer</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_81747b4426a9882884c1f83eda78844f">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <div class="img-wrap">
                                                    <a href="#">
                                                        <img src="images/bertha.jpg" class="img-polaroid"
                                                             alt="Johnny Thompson" title="Johnny Thompson">
                                                    </a>
                                                    <ul class="socials">
                                                        <li class="twitter">
                                                            <a href="http://twitter.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="facebook">
                                                            <a href="http://facebook.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="google-plus">
                                                            <a href="https://plus.google.com/" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="pinterest">
                                                            <a href="https://www.pinterest.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                        <li class="linkedin">
                                                            <a href="https://www.linkedin.com" target="_blank"
                                                               rel="nofollow">
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="info">
                                                    <div class="name">
                                                        <h3><a href="#">BERTHA ARTHUR</a></h3>
                                                    </div>
                                                    <div class="position">Executive Secretary</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
    @if(sizeof($historicalPlayers) > 0 || sizeof($currentPlayers) > 0)
        <div class="tm-bottom-b-box tm-full-width  " id="players">
            <div class="uk-container uk-container-center">
                <section id="tm-bottom-b" class="tm-bottom-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}"
                         data-uk-grid-margin="">
                    <div class="uk-width-1-1 uk-row-first">
                        <div class="uk-panel">
                            <div class="our-history-wrap">
                                <div class="our-history-title">
                                    <h2>Our <span>Players</span></h2>
                                </div>
                                <div class="uk-container uk-container-center match-article ">
                                    <div class="uk-grid">
                                        @if(sizeof($currentPlayers) > 0)
                                            <div class="uk-width-1-1">
                                                <div class="match-gallery">
                                                    <div class="uk-slidenav-position" data-uk-slider="">
                                                        <div class="uk-slider-container">

                                                            <div class="big-title" style="font-size: 20px;">Current
                                                                <span>Players</span></div>
                                                            <div class="match-slider-btn">
                                                                <a draggable="false" href="../index.html"
                                                                   class="uk-slidenav uk-slidenav-previous"
                                                                   data-uk-slider-item="previous"></a>
                                                                <a draggable="false" href="../index.html"
                                                                   class="uk-slidenav uk-slidenav-next"
                                                                   data-uk-slider-item="next"></a>
                                                            </div>
                                                            <ul class="uk-slider uk-grid uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-2">
                                                                @foreach($currentPlayers as $currentPlayer)
                                                                    <li>
                                                                        <div class="player-article">

                                                                            <div class="wrapper">
                                                                                <div class="img-wrap">
                                                                                    <a href="#">
                                                                                        <img
                                                                                            src="data:{{ $currentPlayer->image_type }};base64, {{$currentPlayer->image}}"
                                                                                            class="img-polaroid uk-responsive-height"
                                                                                            alt="{{ $currentPlayer->name }}"
                                                                                            title="J{{ $currentPlayer->name }}">
                                                                                    </a>
                                                                                    <!--
                                                                                        <ul class="socials">
                                                                                            <li class="twitter">
                                                                                                <a href="http://twitter.com/" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="facebook">
                                                                                                <a href="http://facebook.com/" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="google-plus">
                                                                                                <a href="https://plus.google.com/" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="pinterest">
                                                                                                <a href="https://www.pinterest.com" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="linkedin">
                                                                                                <a href="https://www.linkedin.com" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    -->
                                                                                </div>
                                                                                <div class="info"
                                                                                     style="max-width: 235px; background-color:black">
                                                                                    <div class="name">
                                                                                        <h3><a href="#"
                                                                                               style="color:#00afd8">{{ $currentPlayer->name }}</a>
                                                                                        </h3>
                                                                                    </div>
                                                                                    <div class="position"
                                                                                         style="color:white; line-height:3px">{{ $currentPlayer->position }}</div>
                                                                                    <div class="position"
                                                                                         style="color:white">{{ $currentPlayer->team }} </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if(sizeof($historicalPlayers) > 0)
                                            <div class="uk-width-1-1">
                                                <div class="match-gallery">
                                                    <div class="uk-slidenav-position" data-uk-slider="">
                                                        <div class="uk-slider-container">

                                                            <div class="big-title" style="font-size: 20px">Historical
                                                                <span>Players</span></div>
                                                            <div class="match-slider-btn">
                                                                <a draggable="false" href="../index.html"
                                                                   class="uk-slidenav uk-slidenav-previous"
                                                                   data-uk-slider-item="previous"></a>
                                                                <a draggable="false" href="../index.html"
                                                                   class="uk-slidenav uk-slidenav-next"
                                                                   data-uk-slider-item="next"></a>
                                                            </div>
                                                            <ul class="uk-slider uk-grid uk-width-large-1-3 uk-width-medium-1-3 uk-width-small-1-2">
                                                                @foreach($historicalPlayers as $currentPlayer)
                                                                    <li>
                                                                        <div class="player-article">

                                                                            <div class="wrapper">
                                                                                <div class="img-wrap">
                                                                                    <a href="#">
                                                                                        <img
                                                                                            src="data:{{ $currentPlayer->image_type }};base64, {{$currentPlayer->image}}"
                                                                                            class="img-polaroid uk-responsive-height"
                                                                                            alt="{{ $currentPlayer->name }}"
                                                                                            title="J{{ $currentPlayer->name }}">
                                                                                    </a>
                                                                                    <!--
                                                                                        <ul class="socials">
                                                                                            <li class="twitter">
                                                                                                <a href="http://twitter.com/" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="facebook">
                                                                                                <a href="http://facebook.com/" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="google-plus">
                                                                                                <a href="https://plus.google.com/" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="pinterest">
                                                                                                <a href="https://www.pinterest.com" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                            <li class="linkedin">
                                                                                                <a href="https://www.linkedin.com" target="_blank" rel="nofollow">
                                                                                                </a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    -->
                                                                                </div>
                                                                                <div class="info"
                                                                                     style="max-width: 235px; background-color:black">
                                                                                    <div class="name">
                                                                                        <h3><a href="#"
                                                                                               style="color:#00afd8">{{ $currentPlayer->name }}</a>
                                                                                        </h3>
                                                                                    </div>
                                                                                    <div class="position"
                                                                                         style="color:white; line-height:3px">{{ $currentPlayer->position }}</div>
                                                                                    <div class="position"
                                                                                         style="color:white">{{ $currentPlayer->team }} </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    @endif

    <div class="tm-top-e-box tm-full-width  va-main-next-match" id="services">
        <div class="uk-container uk-container-center">
            <section id="tm-top-e" class="tm-top-e uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}"
                     data-uk-grid-margin="">

                <div class="uk-width-1-1">
                    <div class="uk-panel">
                        <div class="our-team-main-wrap">
                            <div class="uk-container uk-container-center">
                                <div class="uk-grid" data-uk-grid-match="">
                                    <div class="uk-width-medium-8-10 uk-width-small-1-1 uk-push-1-10">
                                        <div class="our-team-wrap">
                                            <div class="our-team-title">
                                                <h3>Our <span style="color:white">Services</span></h3>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_2a195f12da9f3f36da06e6097be7e451">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <h3 style="color: #00afd8; text-transform: uppercase; text-align: center; background-color: #1b1e21; margin: 0 0 3px 0 !important;">
                                                    Scouting</h3>
                                                <p class="team-name"
                                                   style=" padding-top: 5px; padding-bottom: 5px; color: black;background-color: whitesmoke; padding-left: 5px; padding-right: 5px; text-align: center; margin-top: 2px !important;">
                                                    Through staged matches and tournaments, we scout for and identify
                                                    young gifted players for the international football market</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_2a195f12da9f3f36da06e6097be7e451">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <h3 style="color: #00afd8; text-transform: uppercase; text-align: center; background-color: #1b1e21; margin: 0 0 3px 0 !important;">
                                                    Training</h3>
                                                <p class="team-name"
                                                   style=" padding-top: 5px; padding-bottom: 5px; color: black;background-color: whitesmoke; padding-left: 5px; padding-right: 5px; text-align: center; margin-top: 2px !important;">The
                                                    agency houses and provides training facilities for our players to
                                                    develop their inherent skills and prepare them for targeted football
                                                    clubs abroad.</p>
                                            </div>
                                        </div>
                                    </div>


                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_2a195f12da9f3f36da06e6097be7e451">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <h3 style="color: #00afd8; text-transform: uppercase; text-align: center; background-color: #1b1e21; margin: 0 0 3px 0 !important;">
                                                    Transfer deals</h3>
                                                <p class="team-name"
                                                   style=" padding-top: 5px; padding-bottom: 5px; color: black;background-color: whitesmoke; padding-left: 5px; padding-right: 5px; text-align: center; margin-top: 2px !important;">We
                                                    broker lucrative transfer deals for talented players; deals that are
                                                    not only financially sound but also protect the future interests and
                                                    prospects of the players we represent</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_2a195f12da9f3f36da06e6097be7e451">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <h3 style="color: #00afd8; text-transform: uppercase; text-align: center; background-color: #1b1e21; margin: 0 0 3px 0 !important;">
                                                    Management</h3>
                                                <p class="team-name"
                                                   style=" padding-top: 5px; padding-bottom: 5px; color: black;background-color: whitesmoke; padding-left: 5px; padding-right: 5px; text-align: center; margin-top: 2px !important;">Our
                                                    job isn’t done when the deal is signed; we continue to manage our
                                                    players and ensure they are being well catered for at their
                                                    respective clubs. We offer financial, branding and PR advisory
                                                    services as well as emotional support during the often gruelling
                                                    football seasons.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class=" uk-width-large-1-4 uk-width-medium-1-3 uk-width-small-1-2 uk-width-1-2 player-item tt_2a195f12da9f3f36da06e6097be7e451">
                                        <div class="player-article">
                                            <div class="wrapper">
                                                <h3 style="color: #00afd8; text-transform: uppercase; text-align: center; background-color: #1b1e21; margin: 0 0 3px 0 !important;">
                                                    3rd party
                                                    engagements</h3>
                                                <p class="team-name"
                                                   style="color: black;background-color: whitesmoke; padding-left: 5px; padding-right: 5px; text-align: center; margin-top: 2px !important; padding-top: 5px; padding-bottom: 5px">ArthurLegacy also deals
                                                    in events, communications, sponsorship packages and marketing</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
    @if(sizeof($news) > 0)
        <div class="tm-top-f-box tm-full-width  va-main-our-news" id="news" style="padding-bottom:70px">
            <div class="uk-container uk-container-center">
                <section id="tm-top-f" class="tm-top-f uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}"
                         data-uk-grid-margin="">
                    <div class="uk-width-1-1">
                        <div class="uk-panel">
                            <div class="uk-container uk-container-center">
                                <div class="uk-grid uk-grid-collapse" data-uk-grid-match="">
                                    <div class="uk-width-1-1" style="padding-bottom: 40px">
                                        <div class="our-news-title">
                                            <h3>Our <span>News</span></h3>
                                        </div>
                                    </div>
                                    @foreach($news as $_news)
                                        <article
                                            class="uk-width-large-1-2 uk-width-medium-1-1 uk-width-small-1-1 our-news-article"
                                            data-uk-grid-match="" style="padding-bottom:70px">
                                            <div class="img-wrap uk-cover-background uk-position-relative"
                                                 style="background-image: url( 'data:{{ $_news->image_type }};base64, {{$_news->image}}'); min-height: 280px;">
                                                <a href="#"></a>
                                                <img class="uk-invisible"
                                                     src="data:{{ $_news->image_type }};base64, {{$_news->image}}"
                                                     alt="">

                                            </div>
                                            <div style="min-height: 280px;" class="info">
                                                <div class="date">
                                                    {{ date('M d, Y', strtotime($_news->created_at)) }} </div>
                                                <div class="name">
                                                    <h4>
                                                        <a href="{{ env('APP_URL') }}single-news?my-news-id={{ $_news->id }}"> {{ $_news->short_title }} </a>
                                                    </h4>
                                                </div>
                                                <div class="text">
                                                    <p>{{ $_news->short_content }}</p>
                                                    <div class="read-more"><a
                                                            href="{{ env('APP_URL') }}single-news?my-news-id={{ $_news->id }}">Read
                                                            More</a>
                                                    </div>
                                                </div>
                                            </div>

                                        </article>
                                    @endforeach
                                </div>
                            </div>
                            <div class="all-news-btn">

                                <a href="all-news"><span>All News</span></a>

                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    @endif
    <div class="tm-bottom-e-box tm-full-width  " id="contact">
        <div class="uk-container uk-container-center">
            <section id="tm-bottom-e" class="tm-bottom-e uk-grid uk-grid-collapse"
                     data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                <div class="uk-width-1-1">
                    <div class="uk-panel">
                        <div class="map-wrap">

                            <div class="contact-form-wrap uk-flex">
                                <div class="uk-container uk-container-center uk-flex-item-1">
                                    <div class="uk-grid  uk-grid-collapse uk-flex-item-1 uk-height-1-1 uk-nbfc">
                                        <div class="uk-width-5-10 contact-left uk-vertical-align contact-left-wrap">
                                            <div class="contact-info-lines uk-vertical-align-middle">
                                                <div class="item phone"><span class="icon"><i class="uk-icon-phone"></i></span>(+233)246025246
                                                </div>
                                                <div class="item mail"><span class="icon"><i
                                                            class="uk-icon-envelope"></i></span><a
                                                        href="mailto:info@arthurlegacy.com">info@arthurlegacy.com</a>

                                                </div>
                                                <div class="item location"><span class="icon"><i
                                                            class="uk-icon-map-marker"></i></span>No7 Boulevard, Kings
                                                    Cottage, Trasacco Estates, Accra
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-medium-5-10 uk-width-small-1-1 contact-right-wrap">
                                            <div class="contact-form uk-height-1-1">
                                                <div class="uk-position-cover uk-flex uk-flex-column uk-flex-center">
                                                    <div class="contact-form-title">
                                                        <h2>Get in touch</h2>
                                                    </div>
                                                    <div id="aiContactSafe_form_1">
                                                        <div class="aiContactSafe" id="aiContactSafe_mainbody_1">
                                                            <div class="contentpaneopen">
                                                                <div id="aiContactSafeForm">
                                                                    <form method="post" id="adminForm_1"
                                                                          name="adminForm_1">
                                                                        <div id="displayAiContactSafeForm_1">
                                                                            <div class="aiContactSafe"
                                                                                 id="aiContactSafe_contact_form">
                                                                                <div class="aiContactSafe"
                                                                                     id="aiContactSafe_info"></div>
                                                                                <div class="aiContactSafe_row"
                                                                                     id="aiContactSafe_row_aics_name">
                                                                                    <div
                                                                                        class="aiContactSafe_contact_form_field_label_left">
                                                                                        <span
                                                                                            class="aiContactSafe_label"
                                                                                            id="aiContactSafe_label_aics_name"><label
                                                                                                for="aics_name">Name</label></span>&nbsp;
                                                                                        <label
                                                                                            class="required_field">*</label>
                                                                                    </div>
                                                                                    <div
                                                                                        class="aiContactSafe_contact_form_field_right">
                                                                                        <input name="aics_name"
                                                                                               id="aics_name"
                                                                                               class="textbox"
                                                                                               placeholder="Name"
                                                                                               value="" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="aiContactSafe_row"
                                                                                     id="aiContactSafe_row_aics_email">
                                                                                    <div
                                                                                        class="aiContactSafe_contact_form_field_label_left">
                                                                                        <span
                                                                                            class="aiContactSafe_label"
                                                                                            id="aiContactSafe_label_aics_email"><label
                                                                                                for="aics_email">E-mail</label></span>&nbsp;
                                                                                        <label
                                                                                            class="required_field">*</label>
                                                                                    </div>
                                                                                    <div
                                                                                        class="aiContactSafe_contact_form_field_right">
                                                                                        <input name="aics_email"
                                                                                               id="aics_email"
                                                                                               class="email"
                                                                                               placeholder="Email"
                                                                                               value="" type="text">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="aiContactSafe_row"
                                                                                     id="aiContactSafe_row_aics_message">
                                                                                    <div
                                                                                        class="aiContactSafe_contact_form_field_label_left">
                                                                                        <span
                                                                                            class="aiContactSafe_label"
                                                                                            id="aiContactSafe_label_aics_message"><label
                                                                                                for="aics_message">Message</label></span>&nbsp;
                                                                                        <label
                                                                                            class="required_field">*</label>
                                                                                    </div>
                                                                                    <div
                                                                                        class="aiContactSafe_contact_form_field_right">
                                                                                        <textarea name="aics_message"
                                                                                                  id="aics_message"
                                                                                                  cols="40" rows="10"
                                                                                                  class="editbox"
                                                                                                  placeholder="Message"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <div id="aiContactSafeBtns">
                                                                            <div id="aiContactSafeButtons_center"
                                                                                 style="clear:both; display:block; text-align:center;">
                                                                                <table
                                                                                    style="margin-left:auto; margin-right:auto;">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div
                                                                                                id="aiContactSafeSend_loading_1">
                                                                                                &nbsp;
                                                                                            </div>
                                                                                        </td>
                                                                                        <td id="td_aiContactSafeSendButton">
                                                                                            <input
                                                                                                id="aiContactSafeSendButton"
                                                                                                value="Send"
                                                                                                type="submit">
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <br>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script>
                                window.map = false;


                                function initialize() {
                                    var myLatlng = new google.maps.LatLng(5.6538995, -0.1053401);

                                    var mapOptions = {
                                        zoom: 16,
                                        center: myLatlng,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        scrollwheel: false,

                                        streetViewControl: false,
                                        overviewMapControl: false,
                                        mapTypeControl: false

                                    };

                                    window.map = new google.maps.Map(document.getElementById('map'), mapOptions);

                                }

                                google.maps.event.addDomListener(window, 'load', initialize);
                            </script>
                            <div id="map"></div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="bottom-wrapper">
        <img draggable="false" class="uk-responsive-height" src="images/partners.jpg" alt=""
             style="padding-bottom:40px">
        <div class="tm-bottom-f-box  ">
            <div class="uk-container uk-container-center">
                <section id="tm-bottom-f" class="tm-bottom-f uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}"
                         data-uk-grid-margin="">

                    <div class="uk-width-1-1">
                        <div class="uk-panel">
                            <div class="footer-logo">
                                <a href="../index.html"><img src="images/logo.png" style="max-height:50px" alt=""></a>
                            </div>
                            <div class="footer-socials">
                                <div class="social-top">
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-facebook"></span></a>
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-twitter"></span></a>
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-google"></span></a>
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-pinterest"></span></a>
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-youtube"></span></a>
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-instagram"></span></a>
                                    <a href="#"><span class="uk-icon-small uk-icon-hover uk-icon-flickr"></span></a>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <footer id="tm-footer" class="tm-footer">


            <div class="uk-panel">
                <div class="uk-container uk-container-center">
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div class="footer-wrap">
                                <div class="foot-menu-wrap">
                                    <ul class="nav menu">
                                        <li class="item-165"><a href="#about">About</a>
                                        </li>
                                        <li class="item-166"><a href="#players">Players</a>
                                        </li>
                                        <li class="item-167"><a href="#team">Team</a>
                                        </li>
                                        <li class="item-168"><a href="#services">Services</a>
                                        </li>
                                        <li class="item-169"><a href="#news">News</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="copyrights">Copyright © <a href="#home">ArthurLegacy</a>.</div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>

    <div id="offcanvas" class="uk-offcanvas">
        <div class="uk-offcanvas-bar">
            <ul class="uk-nav uk-nav-offcanvas">
                <li><a href="#home">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#team">Team</a></li>
                <li><a href="#players">Players</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#news">News</a></li>
                <li><a href="#contact">Contact</a></li>

            </ul>
        </div>
    </div>
</div>


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/uikit.js"></script>
<script type="text/javascript" src="js/SimpleCounter.js"></script>
<script type="text/javascript" src="js/components/grid.js"></script>
<script type="text/javascript" src="js/components/slider.js"></script>
<script type="text/javascript" src="js/components/slideshow.js"></script>
<script type="text/javascript" src="js/components/slideset.js"></script>
<script type="text/javascript" src="js/components/sticky.js"></script>
<script type="text/javascript" src="js/components/lightbox.js"></script>
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script>

<script type="text/javascript" src="js/theme.js"></script>
</body>
</html>
<!-- Localized -->
