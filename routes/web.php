<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'HomeController@home');

$router->get('/about/{teamId}', 'HomeController@about');
$router->get('/all-news', 'HomeController@allNews');
$router->get('/single-news/{newsId}', 'HomeController@singleNews');
$router->get('/single-news', 'HomeController@singleNews');

$router->get('/admin', 'AdminController@players');
$router->get('/admin/players', 'AdminController@players');
$router->get('/admin/add-player', 'AdminController@addPlayer');
$router->post('/admin/add-player', 'AdminController@addPlayerPost');
$router->get('/admin/delete-player/{playerId}', 'AdminController@deletePlayer');

$router->get('/admin/news', 'AdminController@news');
$router->get('/admin/add-news', 'AdminController@addNews');
$router->post('/admin/add-news', 'AdminController@addNewsPost');
$router->get('/admin/delete-news/{newsId}', 'AdminController@deleteNews');
