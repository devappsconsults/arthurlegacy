<?php

namespace App\Http\Controllers;

use App\News;
use App\NewsImage;
use App\Players;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function home()
    {
        $news = News::where('status','active')->latest()->take(2)->get();
        foreach ($news as $value) {
            $value->image_news = NewsImage::where('news_id',$value->id)->where('status','active')->latest();
        }
        $currentPlayers = Players::where('type','current')->get();
        $historicalPlayers = Players::where('type','historical')->get();
        return view('home', compact('news','currentPlayers','historicalPlayers'));
    }

    public function singleNews(Request $request)
    {
        $latestNews = News::where('status','active')->latest()->take(3)->get();
        foreach ($latestNews as $value) {
            $value->image_news = NewsImage::where('news_id',$value->id)->where('status','active')->latest();
        }

        $news = News::where('status','active')->where('id', $request->get('my-news-id'))->first();
        if($news)
        {
            $news->image_news = NewsImage::where('news_id',$news->id)->where('status','active')->latest();
            return view('single-news',compact('news','latestNews'));
        }
        else
        {
            $news = News::where('status','active')->latest()->take(2)->get();
            foreach ($news as $value) {
                $value->image_news = NewsImage::where('news_id',$value->id)->where('status','active')->latest();
            }
            $currentPlayers = Players::where('type','current')->get();
            $historicalPlayers = Players::where('type','historical')->get();
            return view('home', compact('news','currentPlayers','historicalPlayers'));
        }
    }

    public function allNews()
    {
        $latestNews = News::where('status','active')->latest()->take(3)->get();
        foreach ($latestNews as $value) {
            $value->image_news = NewsImage::where('news_id',$value->id)->where('status','active')->latest();
        }
        $news =  News::where('status','active')->latest()->paginate(6);

        foreach ($news as $value) {
            $value->image_news = NewsImage::where('news_id',$value->id)->where('status','active')->latest();
        }

        return view('all-news', compact('latestNews','news'));
    }

    //
}
