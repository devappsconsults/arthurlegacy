<?php

namespace App\Http\Controllers;

use App\News;
use App\NewsImage;
use App\Players;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function players()
    {
        $players = Players::all();
        return view('admin/template/players', compact('players'));
    }

    public function addPlayer()
    {
        return view('admin/template/add-player');
    }

    public function news()
    {
        $news = News::all();
        return view('admin/template/news', compact('news'));
    }
    public function addNews()
    {
        return view('admin/template/add-news');
    }

    //


    public function addPlayerPost(Request $request)
    {
        $file = $request->file('file');
        //dd($file);
        $temp = file_get_contents($file->getRealPath());
        $blob = base64_encode($temp);

        $player = new Players;
        $player->name = $request->get('name');
        $player->team = $request->get('team');
        $player->position = $request->get('position');
        $player->type = $request->get('type');
        $player->image = $blob;
        $player->image_type = $file->getMimeType();
        $player->save();
        return view('admin/template/add-player', compact('player'));
    }
    public function addNewsPost(Request $request)
    {
        $news = new News;
        $news->title = $request->get('title');
        $news->short_title = str_limit(strip_tags($request->get('title')), 150);
        $news->content = $request->get('content');
        $news->short_content = str_limit(strip_tags($request->get('content')), 150);
        $file = $request->file('file');
        $temp = file_get_contents($file->getRealPath());
        $blob = base64_encode($temp);
        $news->image = $blob;
        $news->image_type = $file->getMimeType();
        $news->save();
        return view('admin/template/add-news', compact('news'));
    }


    public function deletePlayer(Request $request, $playerId)
    {
        $player_delete = Players::find($playerId);
        if($player_delete)
        {
            $player_delete->delete();
            $players = Players::all();
            return view('admin/template/players', compact('player_delete','players'));
        }
        else
        {
            $players = Players::all();
            return view('admin/template/players', compact('players'));
        }
    }
    public function deleteNews(Request $request, $newsId)
    {
        $news_delete = News::find($newsId);
        if($news_delete)
        {
            $news_delete->delete();
            $news = News::all();
            return view('admin/template/news', compact('news_delete','news'));
        }
        else
        {
            $news = News::all();
            return view('admin/template/news', compact('news'));
        }
    }

}
